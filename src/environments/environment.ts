// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBJI70olquUcEKhBy4j5isRmwnnT9DHPWQ',
  authDomain: 'mediclicdoc.firebaseapp.com',
  databaseURL: 'https://mediclicdoc.firebaseio.com',
  projectId: 'mediclicdoc',
  storageBucket: 'mediclicdoc.appspot.com',
  messagingSenderId: '382712366730',
  appId: '1:382712366730:web:f42a19531d01aa75fbcf73',
  measurementId: 'G-91ZMXWFB23'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
