
export class Encuestabio {

    constructor(
        public identificacion: number,
        public name?: string,
        public date?: number,
        public _id?: string,
        public cliente?: string,
        public idclient?: string,
        public usuario?: string,
        public email?: string,
        public ciudad?: string,
        public direccion?: string,
        public celular?: number,
        public temperatura?: number,
        public spais14?: boolean,
        public famcovid?: boolean,
        public emailclient?: string,

        public fiebre?: boolean,

        public tos?: boolean,
        public dolorpecho?: boolean,
        public fatiga?: boolean,
        public drespirar?: boolean,
        public mageneral?: boolean,
        public dolorcabeza?: boolean,
        public dolorgarganta?: boolean,
        public faltagusto?: boolean,
        public ninguno?: boolean,
        public estado?: boolean,
        public dateenc?: number,
        public alerta?: boolean,
        public phone?: number,
        public grupo?: string,
        public idgrupo?: string,
        public tipoenc?: string,
    ) { }

}
