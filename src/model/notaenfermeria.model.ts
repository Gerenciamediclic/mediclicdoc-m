
export class Notaenfer {

    constructor(
        public date: number,
        public hinicio?: number,
        public hend?: number,
        public signosvi?: any,
        public liquiin?: any,
        public liquiout?: any,
        public medicacion?: any,
        public nota?: string,
        public _id?: string,
        public idturno?: string,
        public nombre?: string,
        public edad?: string,
        public diagnostico?: string,
        public diagnostentidadco?: string,
    ) { }

}
