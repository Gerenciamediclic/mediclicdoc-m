
export class Medicacion {

    constructor(
        public medicamento: string,
        public via?: string,
        public dosis?: string,
        public date?: number,
    ) { }

}
