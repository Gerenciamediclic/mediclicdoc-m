export class Colaborador {

    constructor(
        public nombre: string,
        public tipoident?: string,
        public identificacion?: number,
        public email?: string,
        public telefono?: number,
        public direccion?: string,
        public ubicacion?: any,
        public ciudad?: string,
        public date?: number,
        public img?: string,
        public departamento?: string,
        public observacion?: string,
        public _id?: string,
        public cliente?: string,
        public idclient?: string,
        public Estado?: boolean,
        public id?: string
    ) { }

}
