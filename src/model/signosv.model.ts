
export class SignosV {

    constructor(
        public ta: string,
        public fc?: string,
        public fr?: string,
        public date?: number,
        public t?: string,
        public sa02?: string,
    ) { }

}
