
export class Usuario {

    constructor(
        public email: string,
        public password?: string,
        public name?: string,
        public date?: number,
        public img?: string,
        public phone?: number,
        public role?: string,
        public uid?: string,
        public _id?: string,
        public cliente?: string,
        public identificacion?: number,
        public idclient?: string,
        public grupo?: string,
        public idgrupo?: string,
        public version?: string,
    ) { }

}
