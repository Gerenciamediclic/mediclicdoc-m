import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login'

  },
  {
    path: 'notaturno/:id',
    loadChildren: () => import('./pages/notaturno/notaturno.module').then( m => m.NotaturnoPageModule)
  },
  {
    path: 'encuesta',
    loadChildren: () => import('./pages/encuesta/encuesta.module').then( m => m.EncuestaPageModule)
  },
  {
    path: 'newencuesta',
    loadChildren: () => import('./pages/encuesta/newencuesta/newencuesta.module').then( m => m.NewencuestaPageModule)
  },
  {
    path: 'cliencuesta',
    loadChildren: () => import('./pages/cliencuesta/cliencuesta.module').then( m => m.CliencuestaPageModule)
  },
  {
    path: 'visitantes',
    loadChildren: () => import('./pages/visitantes/visitantes.module').then( m => m.VisitantesPageModule)
  }
  // {
  //   path: 'firmas/:id',
  //   loadChildren: () => import('./pages/firmas/firmas.module').then( m => m.FirmasPageModule)
  // }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
