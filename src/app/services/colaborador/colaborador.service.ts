import { Injectable } from '@angular/core';
import { Colaborador } from '../../../model/colaborador.model';
import { map, first } from 'rxjs/operators';
import { AngularFirestoreDocument, AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class ColaboradorService {

 

  private itemsCollection: AngularFirestoreCollection<Colaborador>;
  private itemDoc: AngularFirestoreDocument<Colaborador>;

  constructor(
    public router: Router,
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
  ) { }


  GuardarColaborador ( paciente: Colaborador  ) {
    this.itemsCollection = this.afs.collection<Colaborador>('Colaboradores');
    return this.itemsCollection.add( paciente );
  }

  cargarColaborador( ) {
    this.itemsCollection = this.afs.collection<Colaborador>('Colaboradores');
    return this.itemsCollection.snapshotChanges().pipe(
     map(actions => actions.map(a => {
       const data = a.payload.doc.data() as Colaborador;
       const id = a.payload.doc.id;
       data._id = id;
       return { id, ...data };
     }))
   );
 }

 cargarColaboradorthen( ) {
  this.itemsCollection = this.afs.collection<Colaborador>('Colaboradores');
  return this.itemsCollection.snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Colaborador;
     const id = a.payload.doc.id;
     data._id = id;
     return { id, ...data };
   })), first()
   ).toPromise();

}

 actualizarColaborador( paciente: Colaborador ) {
  this.itemsCollection = this.afs.collection<Colaborador>('Colaboradores');
  this.itemDoc = this.afs.doc<Colaborador>('Colaboradores/' + paciente._id);
  return this.itemDoc.update(paciente);
}


buscardocumento( termino: string) {
  this.itemsCollection = this.afs.collection<Colaborador>('Colaboradores');
  const numero = parseInt(termino, 10);
  return this.afs.collection('Colaboradores', ref => ref.where('identificacion', '==', numero)
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Colaborador;
     const id = a.payload.doc.id;
     data._id = id;
     return ({ id, ...data });
   })), first()
  ).toPromise();
}



 buscarColaboradorid( terminon: string ) {
  this.itemsCollection = this.afs.collection<Colaborador>('Colaboradores');
  this.itemDoc = this.afs.doc<Colaborador>('Colaboradores/' + terminon);
  return this.itemDoc.valueChanges().pipe(
            map( client =>  {
                  return client;
              }), first()
              ).toPromise();
  }

cargarColaboradorthenEsta( ) {
    this.itemsCollection = this.afs.collection<Colaborador>('Colaboradores');
    return this.afs.collection('Colaboradores', ref => ref.where('Estado', '==' , false)
              ).snapshotChanges().pipe(
               map(actions => actions.map(a => {
                 const data = a.payload.doc.data() as Colaborador;
                 const metadata = a.payload.doc.metadata.fromCache;
                 const id = a.payload.doc.id;
                 data._id = id;
                 return { id, ...data, metadata  };
               })), first()
               ).toPromise();
}

cargarColaboradorAct( ) {
  this.itemsCollection = this.afs.collection<Colaborador>('Colaboradores');
  return this.afs.collection('Colaboradores', ref => ref.where('Estado', '==' , false)
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Colaborador;
     const metadata = a.payload.doc.metadata.fromCache;
     const id = a.payload.doc.id;
     data._id = id;
     return { id, ...data, metadata  };
   })), first()
   ).toPromise();
 }

cargarColaboradorcliente( usuario: string) {
  this.itemsCollection = this.afs.collection<Colaborador>('Colaboradores');
  return this.afs.collection('Colaboradores', ref => ref.where('idclient', '==', usuario )
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Colaborador;
     const metadata = a.payload.doc.metadata.fromCache;
     const id = a.payload.doc.id;
     data._id = id;
     return { id, ...data, metadata  };
   })));
 }



borrarcliente( termino: string ) {
  this.itemsCollection = this.afs.collection<Colaborador>('Colaboradores');
  this.itemDoc = this.afs.doc<Colaborador>('Colaboradores/' + termino);
  return this.itemDoc.delete();
}


}
