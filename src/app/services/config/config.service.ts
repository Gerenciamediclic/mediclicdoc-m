import { Injectable, Version } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { map, first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

 
version: Version;
  private itemsCollection: AngularFirestoreCollection<Version>;
  private itemDoc: AngularFirestoreDocument<Version>;

  constructor(
    public router: Router,
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
  ) { }

 buscarConfigid( terminon: string ) {
  this.itemsCollection = this.afs.collection<Version>('config');
  this.itemDoc = this.afs.doc<Version>('config/' + terminon);
  return this.itemDoc.valueChanges().pipe(
            map( client =>  {
                  return client;
              }));
  }
}
