import { Injectable } from '@angular/core';
import { Turno } from '../../../model/turnos.model';
import { map, first } from 'rxjs/operators';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class TurnosService {


  private itemsCollection: AngularFirestoreCollection<Turno>;
  private itemDoc: AngularFirestoreDocument<Turno>;

  constructor(
    public router: Router,
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
  ) { }


  GuardarTurnos( turno: Turno  ) {
    this.itemsCollection = this.afs.collection<Turno>('Turno');
    return this.itemsCollection.add( turno );
  }

  cargarTurnos( ) {
    this.itemsCollection = this.afs.collection<Turno>('Turno');
    return this.itemsCollection.snapshotChanges().pipe(
     map(actions => actions.map(a => {
       const data = a.payload.doc.data() as Turno;
       const id = a.payload.doc.id;
       data._id = id;
       return { id, ...data };
     }))
   );
 }

 cargarTurnosthen( ) {
  this.itemsCollection = this.afs.collection<Turno>('Turno');
  return this.itemsCollection.snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Turno;
     const id = a.payload.doc.id;
     data._id = id;
     return { id, ...data };
   })), first()
   ).toPromise();

}

 actualizarTurnos( turno: Turno ) {
  this.itemsCollection = this.afs.collection<Turno>('Turno');
  this.itemDoc = this.afs.doc<Turno>('Turno/' + turno._id);
  return this.itemDoc.update(turno);
}


buscardocumento( termino: string) {
  this.itemsCollection = this.afs.collection<Turno>('Turno');
  const numero = parseInt(termino, 10);
  return this.afs.collection('Turno', ref => ref.where('identificacion', '==', numero)
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Turno;
     const id = a.payload.doc.id;
     data._id = id;
     return ({ id, ...data });
   })), first()
  ).toPromise();
}



 buscarTurnosid( terminon: string ) {
  this.itemsCollection = this.afs.collection<Turno>('Turno');
  this.itemDoc = this.afs.doc<Turno>('Turno/' + terminon);
  return this.itemDoc.valueChanges().pipe(
            map( client =>  {
                  return client;
              }), first()
              ).toPromise();
  }

cargarTurnosthenEsta( ) {
    this.itemsCollection = this.afs.collection<Turno>('Turno');
    return this.afs.collection('Turno', ref => ref.where('Estado', '==' , false)
              ).snapshotChanges().pipe(
               map(actions => actions.map(a => {
                 const data = a.payload.doc.data() as Turno;
                 const metadata = a.payload.doc.metadata.fromCache;
                 const id = a.payload.doc.id;
                 data._id = id;
                 return { id, ...data, metadata  };
               })), first()
               ).toPromise();
}

cargarTurnosAct( ) {
  this.itemsCollection = this.afs.collection<Turno>('Turno');
  return this.afs.collection('Turno', ref => ref.where('Estado', '==' , false)
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Turno;
     const metadata = a.payload.doc.metadata.fromCache;
     const id = a.payload.doc.id;
     data._id = id;
     return { id, ...data, metadata  };
   })), first()
   ).toPromise();
 }

cargarTurnoscolaborador( usuario: string, status: boolean) {
  this.itemsCollection = this.afs.collection<Turno>('Turno');
  return this.afs.collection('Turno', ref => ref.where('idpersonal', '==', usuario )
  .where('status', '==', status )
  .orderBy('hiniciou', 'asc')
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Turno;
     const metadata = a.payload.doc.metadata.fromCache;
     const id = a.payload.doc.id;
     data._id = id;
     return { id, ...data, metadata  };
   })));
 }



borrarTurnos( termino: string ) {
  this.itemsCollection = this.afs.collection<Turno>('Turno');
  this.itemDoc = this.afs.doc<Turno>('Turno/' + termino);
  return this.itemDoc.delete();
}

cargarTurnospaciente( usuario: string) {
  this.itemsCollection = this.afs.collection<Turno>('Turno');
  return this.afs.collection('Turno', ref => ref.where('idpaciente', '==', usuario )
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Turno;
     const metadata = a.payload.doc.metadata.fromCache;
     const id = a.payload.doc.id;
     data._id = id;
     return { id, ...data, metadata  };
   })));
 }


}
