import { Injectable } from '@angular/core';
import { Encuestabio } from '../../../model/encuestabio.model.';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { first, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EncuestabioService {

  
  public activos: Encuestabio;
  private itemsCollection: AngularFirestoreCollection<Encuestabio>;
  private itemDoc: AngularFirestoreDocument<Encuestabio>;


  constructor(
    public router: Router,
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
  ) { }

  crearEncuestabio( usuario: Encuestabio  ) {
    this.itemsCollection = this.afs.collection<Encuestabio>('Encuestabio');
    return this.itemsCollection.add( usuario );
 }

  cargarEncuestabiouser( ) {
    this.itemsCollection = this.afs.collection<Encuestabio>('Encuestabio');
    return this.itemsCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Encuestabio;
        const id = a.payload.doc.id;
        // data._id = id;
        return { id, ...data };
      })), first()
      ).toPromise();
    }

  buscarEncuestabio( terminon: string ) {
    this.itemsCollection = this.afs.collection<Encuestabio>('Encuestabio');
    this.itemDoc = this.afs.doc<Encuestabio>('Encuestabio/' + terminon);
    return this.itemDoc.valueChanges().pipe(
           map( client =>  {
                 return(client);
            }));
  }

  buscarEncuestabioPro( terminon: string ) {
    this.itemsCollection = this.afs.collection<Encuestabio>('Encuestabio');
    this.itemDoc = this.afs.doc<Encuestabio>('Encuestabio/' + terminon);
    return this.itemDoc.valueChanges().pipe(
              map( client =>  {
                 return(client);
                }), first()
    ).toPromise();
  }

  buscarEncuestabiothen( terminon: string ) {
    this.itemsCollection = this.afs.collection<Encuestabio>('Encuestabio');
    this.itemDoc = this.afs.doc<Encuestabio>('Encuestabio/' + terminon);
    return this.itemDoc.valueChanges().pipe(
        map( client =>  {
          return(client);
         }), first()
    ).toPromise();
}

buscarEncuestabioclienteini( termino: string) {
  console.log('termino', termino);
  this.itemsCollection = this.afs.collection<Encuestabio>('Encuestabio');
  return this.afs.collection('Encuestabio', ref => ref.where('idclient', '==', termino).where('estado', '==' , false)
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Encuestabio;
     const id = a.payload.doc.id;
     data._id = id;
     return ({ id, ...data });
   })));
}
buscarEncuestabioclientefin( termino: string) {
  console.log('termino', termino);
  this.itemsCollection = this.afs.collection<Encuestabio>('Encuestabio');
  return this.afs.collection('Encuestabio', ref => ref.where('idclient', '==', termino).where('estado', '==' , true)
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Encuestabio;
     const id = a.payload.doc.id;
     data._id = id;
     return ({ id, ...data });
   })));
}

buscarEncuestabioclienteemail( cliente: string, email: string ) {
  console.log('termino', cliente);
  this.itemsCollection = this.afs.collection<Encuestabio>('Encuestabio');
  return this.afs.collection('Encuestabio', ref => ref.where('idclient', '==', cliente).where('email', '==' , email)
  .where('tipoenc', '==' , 'Auto').orderBy('date', 'desc')
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Encuestabio;
     const id = a.payload.doc.id;
     data._id = id;
     return ({ id, ...data });
   })));
}

buscarEncuestabioclienteemailv( cliente: string, email: string ) {
  console.log('termino', cliente);
  this.itemsCollection = this.afs.collection<Encuestabio>('Encuestabio');
  return this.afs.collection('Encuestabio', ref => ref.where('idclient', '==', cliente).where('email', '==' , email)
  .where('tipoenc', '==' , 'Visitante').orderBy('date', 'desc')
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Encuestabio;
     const id = a.payload.doc.id;
     data._id = id;
     return ({ id, ...data });
   })));
}

actualizarusuario( encuestabio: Encuestabio ) {
  this.itemsCollection = this.afs.collection<Encuestabio>('Encuestabio');
  this.itemDoc = this.afs.doc<Encuestabio>('Encuestabio/' + encuestabio._id);
  return this.itemDoc.update(encuestabio);
}

}
