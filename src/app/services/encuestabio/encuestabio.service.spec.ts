import { TestBed } from '@angular/core/testing';

import { EncuestabioService } from './encuestabio.service';

describe('EncuestabioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EncuestabioService = TestBed.get(EncuestabioService);
    expect(service).toBeTruthy();
  });
});
