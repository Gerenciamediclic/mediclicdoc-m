import { TestBed } from '@angular/core/testing';

import { NotaenferService } from './notaenfer.service';

describe('NotaenferService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotaenferService = TestBed.get(NotaenferService);
    expect(service).toBeTruthy();
  });
});
