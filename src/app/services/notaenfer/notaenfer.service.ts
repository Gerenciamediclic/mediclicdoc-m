import { Injectable } from '@angular/core';
import { Notaenfer } from '../../../model/notaenfermeria.model';

import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { map, first } from 'rxjs/operators';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class NotaenferService {

  private itemsCollection: AngularFirestoreCollection<Notaenfer>;
  private itemDoc: AngularFirestoreDocument<Notaenfer>;

  constructor(
    public router: Router,
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
  ) { }


  GuardarNotaenfers( notaenfer: Notaenfer  ) {
    this.itemsCollection = this.afs.collection<Notaenfer>('Notaenfer');
    return this.itemsCollection.add( notaenfer );
  }

  cargarNotaenfers( ) {
    this.itemsCollection = this.afs.collection<Notaenfer>('Notaenfer');
    return this.itemsCollection.snapshotChanges().pipe(
     map(actions => actions.map(a => {
       const data = a.payload.doc.data() as Notaenfer;
       const id = a.payload.doc.id;
       data._id = id;
       return { id, ...data };
     }))
   );
 }

 cargarNotaenfersthen( ) {
  this.itemsCollection = this.afs.collection<Notaenfer>('Notaenfer');
  return this.itemsCollection.snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Notaenfer;
     const id = a.payload.doc.id;
     data._id = id;
     return { id, ...data };
   })), first()
   ).toPromise();

}

 actualizarNotaenfers( notaenfer: Notaenfer ) {
  this.itemsCollection = this.afs.collection<Notaenfer>('Notaenfer');
  this.itemDoc = this.afs.doc<Notaenfer>('Notaenfer/' + notaenfer._id);
  return this.itemDoc.update(notaenfer);
}


buscardocumento( termino: string) {
  this.itemsCollection = this.afs.collection<Notaenfer>('Notaenfer');
  return this.afs.collection('Notaenfer', ref => ref.where('idturno', '==', termino)
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Notaenfer;
     const id = a.payload.doc.id;
     data._id = id;
     return ({ id, ...data });
   })), first()
  ).toPromise();
}



 buscarNotaenfersid( terminon: string ) {
  this.itemsCollection = this.afs.collection<Notaenfer>('Notaenfer');
  this.itemDoc = this.afs.doc<Notaenfer>('Notaenfer/' + terminon);
  return this.itemDoc.valueChanges().pipe(
            map( client =>  {
                  return client;
              }), first()
              ).toPromise();
  }

cargarNotaenfersthenEsta( ) {
    this.itemsCollection = this.afs.collection<Notaenfer>('Notaenfer');
    return this.afs.collection('Notaenfer', ref => ref.where('Estado', '==' , false)
              ).snapshotChanges().pipe(
               map(actions => actions.map(a => {
                 const data = a.payload.doc.data() as Notaenfer;
                 const metadata = a.payload.doc.metadata.fromCache;
                 const id = a.payload.doc.id;
                 data._id = id;
                 return { id, ...data, metadata  };
               })), first()
               ).toPromise();
}

cargarNotaenfersAct( ) {
  this.itemsCollection = this.afs.collection<Notaenfer>('Notaenfer');
  return this.afs.collection('Notaenfer', ref => ref.where('Estado', '==' , false)
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Notaenfer;
     const metadata = a.payload.doc.metadata.fromCache;
     const id = a.payload.doc.id;
     data._id = id;
     return { id, ...data, metadata  };
   })), first()
   ).toPromise();
 }

cargarNotaenferscolaborador( usuario: string) {
  this.itemsCollection = this.afs.collection<Notaenfer>('Notaenfer');
  return this.afs.collection('Notaenfer', ref => ref.where('idpersonal', '==', usuario )
  .orderBy('hiniciou', 'asc')
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Notaenfer;
     const metadata = a.payload.doc.metadata.fromCache;
     const id = a.payload.doc.id;
     data._id = id;
     return { id, ...data, metadata  };
   })));
 }



borrarNotaenfers( termino: string ) {
  this.itemsCollection = this.afs.collection<Notaenfer>('Notaenfer');
  this.itemDoc = this.afs.doc<Notaenfer>('Notaenfer/' + termino);
  return this.itemDoc.delete();
}

cargarNotaenferspaciente( usuario: string) {
  this.itemsCollection = this.afs.collection<Notaenfer>('Notaenfer');
  return this.afs.collection('Notaenfer', ref => ref.where('idpaciente', '==', usuario )
  ).snapshotChanges().pipe(
   map(actions => actions.map(a => {
     const data = a.payload.doc.data() as Notaenfer;
     const metadata = a.payload.doc.metadata.fromCache;
     const id = a.payload.doc.id;
     data._id = id;
     return { id, ...data, metadata  };
   })));
 }

}
