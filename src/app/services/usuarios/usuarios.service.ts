import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { map , first } from 'rxjs/operators';
import { Usuario } from '../../../model/usuario.model';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  
  public usuario: Usuario;
  token: string;
  private itemsCollection: AngularFirestoreCollection<Usuario>;
  private itemDoc: AngularFirestoreDocument<Usuario>;

  constructor(
    public router: Router,
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private navcontr: NavController
  ) {

    this.afAuth.authState.subscribe( user => {
      if ( !user ) {
        return;
      }
      this.cargarStorage();
      this.buscarUsuarios(user.email).then((resp) => {
        this.usuario = resp[0];
        this.usuario.email = user.email;
        this.usuario.date = new Date().getTime();
        this.usuario.uid = user.uid;
        this.usuario.version = '1.0.2';
        this.guardarStorage(this.usuario);
        this.navcontr.navigateRoot('/tabs/tabs/encuesta');
      });
    });
    this.cargarStorage();
  }

  estaLogueado() {
      return ( this.token.length > 5 ) ? true : false;
  }

   cargarStorage() {
     if ( localStorage.getItem('token')) {
       this.token = localStorage.getItem('token');
       this.usuario = JSON.parse( localStorage.getItem('usuariomovil') );
     } else {
       this.token = '';
       this.usuario = null;
     }
   }

   guardarStorage( usuario: Usuario) {
    localStorage.setItem('token', usuario.uid );
    localStorage.setItem('usuariomovil', JSON.stringify(usuario) );
   }

  logout() {
    return this.afAuth.signOut()
          .then( (fun) => {
            localStorage.removeItem('token');
            localStorage.removeItem('usuariomovil');
            this.usuario = null;
            this.token = '';
            // this.router.navigate(['/login']);
             // location.reload(true);
          } ).catch ((err) => {
            console.log ('error', err );
          });
    }

  loginGoogle( ) {
   return this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  login( usuario: Usuario, recordar: boolean = false ) {
    return new Promise ((resolve, reject ) => {
       this.afAuth.signInWithEmailAndPassword (usuario.email, usuario.password)
      .then( userData => resolve(userData),
       err => reject (err));
     });
  }

  // GuardarUsuario( usuario: Usuario ) {

  // this.itemsCollection = this.afs.collection<Usuario>('users');

  //     return this.itemsCollection.valueChanges()
  //                 .map( (usuarios: Usuario[] ) => {
  //                 });

  // }

   crearUsuario( usuario: Usuario  ) {
     this.itemsCollection = this.afs.collection<Usuario>('users');
     return this.itemsCollection.add( usuario );
  }

   cargarUsuarios( ) {
      this.itemsCollection = this.afs.collection<Usuario>('users');
      return this.itemsCollection.snapshotChanges().pipe(
       map(actions => actions.map (a => {
            const data = a.payload.doc.data() as Usuario;
            const id = a.payload.doc.id;
            data._id = id;
            return { id, ...data };
          }))
        );
  }

  buscarUsuarios( usuario: string ) {
    return this.afs.collection('users', ref => ref.where('email', '==', usuario )).snapshotChanges().pipe(
      map(actions => actions.map(a => {
              const data = a.payload.doc.data() as Usuario;
              const id = a.payload.doc.id;
              data._id = id;
              return { id, ...data };
            })), first()
            ).toPromise();
          }
 // borrarUsuario( id: string ) {
  borrarUsuario( termino: string ) {
    this.itemsCollection = this.afs.collection<Usuario>('users');
    this.itemDoc = this.afs.doc<Usuario>('users/' + termino);
    return this.itemDoc.delete();
  }

  registerUser(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.createUserWithEmailAndPassword(email, pass)
      .then( userData => resolve(userData),
      err => reject (err));
    });
  }
  actualizarusuario( usuario: Usuario ) {
     this.itemsCollection = this.afs.collection<Usuario>('users');
     this.itemDoc = this.afs.doc<Usuario>('users/' + usuario._id);
     return this.itemDoc.update(usuario);
  }

  getAuth () {
     return this.afAuth.authState.pipe ( auth => auth );
  }

  resetPassword(email: string) {
     return this.afAuth.sendPasswordResetEmail(email);
  }

  buscarusuario( usuario: string ) {
    return this.afs.collection('users', ref => ref.where('email', '==', usuario )).snapshotChanges().pipe(
      map(actions => actions.map(a => {
              const data = a.payload.doc.data() as Usuario;
              const id = a.payload.doc.id;
              data._id = id;
              return { id, ...data };
            })));
          }
}

