import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab1/tab1.module').then(m => m.Tab1PageModule)
          }
        ]
      },
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab2/tab2.module').then(m => m.Tab2PageModule)
          }
        ]
      },
      {
        path: 'tab3',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab3/tab3.module').then(m => m.Tab3PageModule)
          }
        ]
      },
      {
        path: 'encuesta',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../encuesta/encuesta.module').then(m => m.EncuestaPageModule)
          }
        ]
      },
      {
        path: 'cliencuesta',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../cliencuesta/cliencuesta.module').then(m => m.CliencuestaPageModule)
          }
        ]
      },
      {
        path: 'visitantes',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../visitantes/visitantes.module').then(m => m.VisitantesPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/encuesta',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/encuesta',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
