import { Component, OnInit } from '@angular/core';
import { UiserviceService } from '../../services/Uiservice/uiservice.service';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import * as moment from 'moment';
import { SignosV } from '../../../model/signosv.model';
import { Liquiin } from '../../../model/liquiin.model';
import { Liquiout } from '../../../model/liqiout.model';
import { Medicacion } from '../../../model/medicacion.model';
import { ActivatedRoute, Router } from '@angular/router';
import { NotaenferService } from '../../services/notaenfer/notaenfer.service';
import { Notaenfer } from 'src/model/notaenfermeria.model';
import { TurnosService } from '../../services/turnos/turnos.service';
import { Turno } from '../../../model/turnos.model';
import { FirmasPage } from '../firmas/firmas.page';

@Component({
  selector: 'app-notaturno',
  templateUrl: './notaturno.page.html',
  styleUrls: ['./notaturno.page.scss'],
})
export class NotaturnoPage implements OnInit {

  initturno: boolean;
  hora: string;
  idturno: string;
  imagen: any;

  signosvs: SignosV [] = [];
  signosv = new SignosV (
    ''
  );

  liquiins: Liquiin [] = [];
  liquiin1 = new Liquiin(
    ''
  );

  liquiiouts: Liquiout [] = [];
  liquiiout = new Liquiout(
    ''
  );

 medicamentos: Medicacion [] = [];
 medicamento = new Medicacion (
   ''
 );

 notaenfers: Turno [] = [];
 notaenfer = new Turno (
   null
 );

  constructor(
    private uiservice: UiserviceService,
    public alertController: AlertController,
    public activateRoute: ActivatedRoute,
    private turnoservice: TurnosService,
    private router: Router,
    private modalcontrol: ModalController,
    private navcontr: NavController
  ) {
    activateRoute.params.subscribe( params => {
      this.idturno = params.id;
      this.buscarnota(this.idturno);
      // this.grupo = params.id;
      // this.pttosmostrar();
    });
  }

  ngOnInit() {
    this.initturno = false;
  }

  addsignos() {
    console.log('signos');
    if (this.initturno === true  ) {
      this.hora = moment().format('LT');
      this.alertaddsignos();
      } else {
        this.uiservice.alertainformativa('¡Debe Iniciar Turno!');
      }
  }

  liquidin() {
    console.log('liquidin');
    if (this.initturno === true  ) {
      this.hora = moment().format('LT');
      this.alertaliquiin();

      } else {
        this.uiservice.alertainformativa('¡Debe Iniciar Turno!');
      }
  }

  liquiout() {
    console.log('liquiout');
    if (this.initturno === true  ) {
       // this.presentAlertCheckbox();
       this.hora = moment().format('LT');
       this.alertaliquiout();
      } else {
        this.uiservice.alertainformativa('¡Debe Iniciar Turno!');
      }
  }

  medicina() {
    console.log('medicina');
    if (this.initturno === true  ) {
      this.hora = moment().format('LT');
      this.alertMedicantos();
      } else {
        this.uiservice.alertainformativa('¡Debe Iniciar Turno!');
      }
  }

  estadoturno() {
    if (this.initturno === false  ) {
      this.notaenfer.hinicioturno = new Date().getTime();
      this.notaenfer.estado = true;
      this.notaenfer.colorult = 'success';
      const dataa = JSON.parse (JSON.stringify (this.notaenfer));
      this.turnoservice.actualizarTurnos(dataa).then( resp => {
        console.log('Guardada', resp);
        this.initturno = true;
        console.log('Guardada', this.notaenfer);
      });
    } else {
      console.log('Finalizar', this.notaenfer);
      if ( this.notaenfer.signosvi !== undefined ) {
        if ( this.notaenfer.liquiin !== undefined ) {
          if ( this.notaenfer.liquiout !== undefined ) {
            if ( this.notaenfer.medicacion !== undefined ) {
              if ( this.notaenfer.nota !== undefined ) {
                  const data = JSON.parse (JSON.stringify (this.notaenfer));
                  this.turnoservice.actualizarTurnos(data).then(resp => {
                  console.log('actualizado');
                  });
                  if ( this.notaenfer.firmapro !== undefined ) {
                      if ( this.notaenfer.firmafam !== undefined ) {
                        this.uiservice.alertainformativa('¡Finalizamos con Exito!');
                        this.notaenfer.status = false;
                        this.notaenfer.hsalida = new Date().getTime();
                        const data1 = JSON.parse (JSON.stringify (this.notaenfer));
                        this.turnoservice.actualizarTurnos(data1).then(resp => {
                        console.log('actualizado');
                        this.navcontr.navigateRoot('/tabs/tabs/tab2');
                        });


                    } else {
                      this.uiservice.alertainformativa('¡Debe Registra la firma Familiar!');
                    }
                  } else {
                    this.uiservice.alertainformativa('¡Debe Registra la firma Profesional!');
                  }
              } else {
                this.uiservice.alertainformativa('¡Debe Ingresar Nota!');
              }
            } else {
              this.uiservice.alertainformativa('¡Debe Ingresar Medicamentos!');
            }
          } else {
            this.uiservice.alertainformativa('¡Debe Ingresar Liquidos El!');
          }
        } else {
          this.uiservice.alertainformativa('¡Debe Ingresar Liquidos Ad!');
        }
      } else  {
        this.uiservice.alertainformativa('¡Debe Ingresar Signos Vitales!');
      }
    }
  }

  buscarnota( id: string) {
      this.turnoservice.buscarTurnosid(id).then(respdoc => {
            this.notaenfer = respdoc;
            this.notaenfer._id = id;
            console.log(this.notaenfer);
            if (this.notaenfer.estado === false  ) {
               console.log('no esta');
               this.initturno = false;
             } else {
            console.log('si esta');
            if  ( this.notaenfer.signosvi !== undefined ) {
                this.signosvs = this.notaenfer.signosvi;
              }
            if ( this.notaenfer.liquiin !== undefined ) {
              this.liquiins = this.notaenfer.liquiin;
              }
            if ( this.notaenfer.liquiout !== undefined ) {
                this.liquiiouts = this.notaenfer.liquiout;
                }
            if ( this.notaenfer.medicacion !== undefined ) {
                  this.medicamentos = this.notaenfer.medicacion;
              }
            this.initturno = true;
            }
  });
  }

  async alertaddsignos() {
    const alert = await this.alertController.create({
      header: 'Signos Vitales',
      subHeader: 'Hora: ' + this.hora,
      inputs: [
        {
          name: 'ta',
          type: 'text',
          placeholder: 'T/A',
          id: 'ta'
        },
        {
          name: 'fc',
          type: 'text',
          id: 'fc',
          placeholder: 'F.C'
        },
        {
          name: 'fr',
          type: 'text',
          id: 'fr',
          placeholder: 'F.R'
        },
        {
          name: 't',
          type: 'text',
          id: 't',
          placeholder: 'T °c'
        },
        {
          name: 'sa02',
          type: 'text',
          id: 'sa02',
          placeholder: 'SA02'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (date) => {
            console.log('Confirm Ok', date);
            this.signosv = date;
            console.log('Confirm F.C', date);
            console.log('this.signosv', this.signosv);
            this.signosv.date = new Date().getTime();
            const dataa = JSON.parse (JSON.stringify (this.signosv));
            this.signosvs = this.signosvs.concat(dataa);
            this.notaenfer.signosvi =  this.signosvs;
            const data = JSON.parse (JSON.stringify (this.notaenfer));
            this.turnoservice.actualizarTurnos(data).then(resp => {
              console.log('actualizado');
            });

          }
        }
      ]
    });

    await alert.present();
  }


  async alertaliquiin() {
    const alert = await this.alertController.create({
      header: 'Liquidos Administrados',
      subHeader: 'Hora: ' + this.hora,
      inputs: [
        {
          name: 'voral',
          type: 'radio',
          label: 'Via Oral',
          value: 'Via Oral',
          checked: true
        },
        {
          name: 'vparental',
          type: 'radio',
          label: 'Parental',
          value: 'Parental'
        },
        {
          name: 'sondas',
          type: 'radio',
          label: 'Sondas',
          value: 'Sondas'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (date) => {
            console.log('Confirm Ok', date);
            this.alertatotal1(date);
           }
        }
      ]
    });

    await alert.present();
  }


  async alertatotal1(dato: string) {
    const alert = await this.alertController.create({
      header: 'Total Liquidos',
      subHeader: 'Hora: ' + this.hora,
      inputs: [
        {
          name: 'total',
          type: 'text',
          placeholder: 'Total Liquidos',
          id: 'total'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (date) => {
            console.log('Confirm Ok', date);
            console.log('Confirm Ok', dato);
            this.liquiin1.tipo = dato;
            this.liquiin1.total = date.total;
            this.liquiin1.date = new Date().getTime();
            // const dataa1 = JSON.parse (JSON.stringify (this.liquiin1));
            this.liquiins = this.liquiins.concat(this.liquiin1);
            this.notaenfer.liquiin =  this.liquiins;
            console.log('Confirm Ok', this.notaenfer);
            const data = JSON.parse (JSON.stringify (this.notaenfer));
            this.turnoservice.actualizarTurnos(data).then(resp => {
               console.log('actualizado');
            });
          }
        }
      ]
    });
    await alert.present();
  }


  async alertaliquiout() {
    const alert = await this.alertController.create({
      header: 'Liquidos Administrados',
      subHeader: 'Hora: ' + this.hora,
      inputs: [
        {
          name: 'orina',
          type: 'radio',
          label: 'Orina',
          value: 'Orina',
          checked: true
        },
        {
          name: 'vomito',
          type: 'radio',
          label: 'Vomito',
          value: 'Vomito'
        },
        {
          name: 'sng',
          type: 'radio',
          label: 'SNG',
          value: 'SNG'
        },
        {
          name: 'heces',
          type: 'radio',
          label: 'Heces',
          value: 'Heces'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (date) => {
            console.log('Confirm Ok', date);
            this.alertatotal2(date);
           }
        }
      ]
    });

    await alert.present();
  }


  async alertatotal2(dato: string) {
    const alert = await this.alertController.create({
      header: 'Total Liquidos',
      subHeader: 'Hora: ' + this.hora,
      inputs: [
        {
          name: 'total',
          type: 'text',
          placeholder: 'Total Liquidos',
          id: 'total'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (date) => {
            console.log('Confirm Ok', date);
            console.log('Confirm Ok', dato);
            this.liquiiout.tipo = dato;
            this.liquiiout.total = date.total;
            this.liquiiout.date = new Date().getTime();
            this.liquiiouts = this.liquiiouts.concat(this.liquiiout);
            this.notaenfer.liquiout =  this.liquiiouts;
            console.log('Confirm Ok', this.notaenfer);
            const data = JSON.parse (JSON.stringify (this.notaenfer));
            this.turnoservice.actualizarTurnos(data).then(resp => {
               console.log('actualizado');
            });


          }
        }
      ]
    });

    await alert.present();
  }



  async alertMedicantos() {
    const alert = await this.alertController.create({
      header: 'Medicacion',
      subHeader: 'Hora: ' + this.hora,
      inputs: [
        {
          name: 'medicamento',
          type: 'text',
          placeholder: 'Medicamento',
          id: 'medicamento'
        },
        {
          name: 'via',
          type: 'text',
          id: 'via',
          placeholder: 'Via'
        },
        {
          name: 'dosis',
          type: 'text',
          id: 'dosis',
          placeholder: 'Dosis'
        },
        
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (date) => {
            console.log('Confirm Ok', date);
            this.medicamento = date;
            console.log('Confirm F.C', date);
            console.log('this.signosv', this.medicamento);
            this.medicamento.date = new Date().getTime();
            const dataa = JSON.parse (JSON.stringify (this.medicamento));
            this.medicamentos = this.medicamentos.concat(dataa);
            this.notaenfer.medicacion =  this.medicamentos;
            const data = JSON.parse (JSON.stringify (this.notaenfer));
            this.turnoservice.actualizarTurnos(data).then(resp => {
              console.log('actualizado');
            });


          }
        }
      ]
    });

    await alert.present();
  }

  async firmaprefesional() {
      const modal = await this.modalcontrol.create({
        component: FirmasPage,
        componentProps: {
          nombre: 'Firma Profesional'
        }
      });
      await modal.present();
      const firma1 = await modal.onDidDismiss();
      this.notaenfer.firmapro = firma1.data;
    }


    async firmafamiliar() {
    const modal = await this.modalcontrol.create({
      component: FirmasPage,
      componentProps: {
        nombre: 'Firma Familiar'
      }
    });
    await modal.present();
    const firma1 = await modal.onDidDismiss();
    this.notaenfer.firmafam = firma1.data;
  }


 async abrirfirmas(nombre: string) {
  const modal = await this.modalcontrol.create({
    component: FirmasPage,
    componentProps: {
      nombre: (nombre)
    }
  });
  await modal.present();
  const data = await modal.onDidDismiss();
}

}
