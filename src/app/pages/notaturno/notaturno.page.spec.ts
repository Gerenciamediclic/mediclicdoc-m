import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NotaturnoPage } from './notaturno.page';

describe('NotaturnoPage', () => {
  let component: NotaturnoPage;
  let fixture: ComponentFixture<NotaturnoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotaturnoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NotaturnoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
