import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotaturnoPageRoutingModule } from './notaturno-routing.module';

import { NotaturnoPage } from './notaturno.page';
import { FirmasPage } from '../firmas/firmas.page';
import { FirmasPageModule } from '../firmas/firmas.module';


@NgModule({
  entryComponents: [
    FirmasPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotaturnoPageRoutingModule,
    FirmasPageModule
  ],
  declarations: [NotaturnoPage]
})
export class NotaturnoPageModule {}
