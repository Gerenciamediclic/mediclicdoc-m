import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotaturnoPage } from './notaturno.page';

const routes: Routes = [
  {
    path: '',
    component: NotaturnoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotaturnoPageRoutingModule {}
