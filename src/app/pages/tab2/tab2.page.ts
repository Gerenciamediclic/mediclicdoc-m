import { Component, OnInit } from '@angular/core';
import { TurnosService } from '../../services/turnos/turnos.service';
import { ColaboradorService } from '../../services/colaborador/colaborador.service';
import { UsuariosService } from '../../services/usuarios/usuarios.service';
import { Usuario } from '../../../model/usuario.model';
import { Turno } from '../../../model/turnos.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page  implements OnInit {

  usuario: Usuario;
  turnos: Turno [] = [];

  constructor(
    private router: Router,
    private turnoservice: TurnosService,
    private colaboservice: ColaboradorService,
    private loginservice: UsuariosService,

  ) {}


  ngOnInit() {
    //this.loginservice.logout();
    this.loginservice.cargarStorage();
    this.usuario = JSON.parse( localStorage.getItem('usuariomovil') );
    console.log('usuario', this.usuario);
    this.colaboservice.buscardocumento(this.usuario.identificacion.toString()).then(resp => {
      if (resp.length >= 1 ) {
      this.turnoservice.cargarTurnoscolaborador(resp[0]._id, true).subscribe(resps => {
      this.turnos = resps;
      console.log('calen', resps);
      });
    }
   });
  }

  // turnodeta(turno: Turno ) {
  //   console.log(turno);
  //   this.router.navigateByUrl('notaturno', turno._id);
  // }

}
