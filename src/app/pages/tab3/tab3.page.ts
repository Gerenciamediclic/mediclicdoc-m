import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../../services/usuarios/usuarios.service';
import { Usuario } from '../../../model/usuario.model';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  usuario: Usuario;

  constructor(
    private loginservice: UsuariosService,
    private navcontr: NavController
  ) {}



  ngOnInit() {
    //this.loginservice.logout();
    this.loginservice.cargarStorage();
    this.usuario = JSON.parse( localStorage.getItem('usuariomovil') );
    console.log('usuario', this.usuario);
  }

  cerrar() {
    this.loginservice.logout();
    this.navcontr.navigateRoot('/login');
  }

}
