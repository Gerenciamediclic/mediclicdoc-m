import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Usuario } from '../../../model/usuario.model';
import { EncuestabioService } from '../../services/encuestabio/encuestabio.service';
import { Encuestabio } from '../../../model/encuestabio.model.';
import { ConfigService } from '../../services/config/config.service';
import { Config } from '../../../model/config.model';


@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.page.html',
  styleUrls: ['./encuesta.page.scss'],
})
export class EncuestaPage implements OnInit {

  usuario: Usuario;
  encuestas: Encuestabio [] = [];
  config = new  Config (
    ''
  );
  config2: any;
  newversion: boolean;


  constructor(
    private navcontr: NavController,
    private encuservice: EncuestabioService,
    private configservice: ConfigService
  ) { }

  ngOnInit() {
    this.usuario = JSON.parse( localStorage.getItem('usuariomovil') );
    console.log('usuario', this.usuario);
    this.encuservice.buscarEncuestabioclienteemail(this.usuario.idclient, this.usuario.email ).subscribe(resp => {
      console.log( 'resp' , resp);
      this.encuestas = resp;
    });
    // this.configservice.buscarConfigid('version').subscribe( resp => {
    //   this.config2 = resp;
    //   if ( this.config2.version === this.usuario.version ) {
    //     console.log( 'igual', this.config2.version);
    //     this.newversion = false;
    //   } else {
    //     this.newversion = true;
    //   }
    // });
  }


  onClick() {
    console.log('nuevaEncuesta');
    this.navcontr.navigateRoot('/newencuesta');
  }

  actualizar() {
    window.open('market://details?id=com.egr2.homecaredoc2', '_system');
  }

}
