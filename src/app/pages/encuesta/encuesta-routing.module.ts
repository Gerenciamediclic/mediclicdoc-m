import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EncuestaPage } from './encuesta.page';

const routes: Routes = [
  {
    path: '',
    component: EncuestaPage
  },
  {
    path: 'newencuesta',
    loadChildren: () => import('./newencuesta/newencuesta.module').then( m => m.NewencuestaPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EncuestaPageRoutingModule {}
