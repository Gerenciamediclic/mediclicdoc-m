import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewencuestaPage } from './newencuesta.page';

describe('NewencuestaPage', () => {
  let component: NewencuestaPage;
  let fixture: ComponentFixture<NewencuestaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewencuestaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewencuestaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
