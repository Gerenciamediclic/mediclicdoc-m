import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewencuestaPage } from './newencuesta.page';

const routes: Routes = [
  {
    path: '',
    component: NewencuestaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewencuestaPageRoutingModule {}
