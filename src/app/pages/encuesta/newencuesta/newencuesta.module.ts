import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewencuestaPageRoutingModule } from './newencuesta-routing.module';

import { NewencuestaPage } from './newencuesta.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewencuestaPageRoutingModule
  ],
  declarations: [NewencuestaPage]
})
export class NewencuestaPageModule {}
