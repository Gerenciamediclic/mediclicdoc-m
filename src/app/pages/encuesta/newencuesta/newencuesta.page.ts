import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Encuestabio } from '../../../../model/encuestabio.model.';
import { UiserviceService } from '../../../services/Uiservice/uiservice.service';
import { EncuestabioService } from '../../../services/encuestabio/encuestabio.service';
import { Usuario } from '../../../../model/usuario.model';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-newencuesta',
  templateUrl: './newencuesta.page.html',
  styleUrls: ['./newencuesta.page.scss'],
})
export class NewencuestaPage implements OnInit {

  todo: any;

  salirsi: boolean;
  salirno: boolean;
  vcovidsi: boolean;
  vcovidno: boolean;
  usuario: Usuario;

  encuestabio = new Encuestabio(
    null
  );

  constructor(
    private uiservice: UiserviceService,
    private encuservice: EncuestabioService,
    private navcontr: NavController
  ) { }

  ngOnInit() {
    this.salirsi = false;
    this.salirno = false;
    this.vcovidsi = false;
    this.vcovidno = false;
    this.usuario = JSON.parse( localStorage.getItem('usuariomovil') );
    console.log('usuario', this.usuario);
  }


  encuestac(f: NgForm) {
    console.log(f);
    
    if ( this.encuestabio.ninguno === undefined ) {
        if ( this.encuestabio.tos === undefined ) {
          if ( this.encuestabio.drespirar === undefined ) {
            if ( this.encuestabio.dolorcabeza === undefined ) {
              if ( this.encuestabio.dolorgarganta === undefined ) {
                if ( this.encuestabio.faltagusto === undefined ) {
                  this.uiservice.alertainformativa('¡Debe seleccionar un Campo!');
                } else {
                  this.encuestabio.alerta = true;
                  this.validar0();
                }
              } else {
                this.encuestabio.alerta = true;
                this.validar0();
              }
            } else {
              this.encuestabio.alerta = true;
              this.validar0();
            }
          } else {
            this.encuestabio.alerta = true;
            this.validar0();
          }
        } else {
          this.encuestabio.alerta = true;
          this.validar0();
        }

    } else {
      this.encuestabio.alerta = false;
      this.validar0();
    }
  }


  validar0() {
    if ( this.salirsi === true) {
      this.encuestabio.spais14 = true;
      this.encuestabio.alerta = true;
      this.validar1();
} else {
    if ( this.salirno === true) {
      this.encuestabio.spais14 = false;
      this.validar1();
    } else {
      this.uiservice.alertainformativa('¡Debe seleccionar un Campo Viaje Al Exterior!');
    }
    }
  }

  validar1() {
    if ( this.vcovidsi === true) {
      this.encuestabio.famcovid = true;
      this.encuestabio.alerta = true;
      this.validartem();
    } else {
   if ( this.vcovidno === true) {
    this.encuestabio.famcovid = false;
    this.validartem();
   } else {
     this.uiservice.alertainformativa('¡Debe seleccionar un Campo COVID!');
   }
  }
  }

  validartem() {
    if ( this.encuestabio.temperatura <= 32  || this.encuestabio.temperatura >= 45) {
      this.uiservice.alertainformativa('¡Rango De temperatura errado!');
    } else {
      if ( this.encuestabio.temperatura >= 38 ) {
        this.uiservice.alertainformativa('!Se Solicita Aislamiento Preventivo!');
        this.encuestabio.alerta = true;
        this.guardarform();
      } else {
        this.guardarform();
      }
  }
}

  guardarform() {
    this.encuestabio.estado = true;
    this.encuestabio.name = this.usuario.name;
    this.encuestabio.identificacion = this.usuario.identificacion;
    this.encuestabio.email = this.usuario.email;
    this.encuestabio.cliente = this.usuario.cliente;
    this.encuestabio.idclient = this.usuario.idclient;
    this.encuestabio.date = new Date().getTime();
    this.encuestabio.phone = this.usuario.phone;
    this.encuestabio.idgrupo  = this.usuario.idgrupo;
    this.encuestabio.grupo  = this.usuario.grupo;
    this.encuestabio.tipoenc = 'Auto';
    const dataa = JSON.parse (JSON.stringify (this.encuestabio));
    this.encuservice.crearEncuestabio(dataa).then(resp => {
      this.uiservice.alertainformativa('¡Se Ha Guardado Con Exito');
      this.navcontr.navigateRoot('/tabs/tabs/encuesta');
    });
  }

  


  salirsie(evnet: any) {
    console.log(evnet);
    if (evnet.detail.checked === true) {
      this.salirno = false;
      this.salirsi = true;
    } else {
      this.salirno = true;
      this.salirsi = false;
    }
    
  }
 
  salirnoe(evnet: any) {
    if (evnet.detail.checked === true) {
      this.salirno = true;
      this.salirsi = false;
    } else {
      this.salirno = false;
      this.salirsi = true;
    }
  }

  vcovidsie(evnet: any) {
    console.log(evnet);
    if (evnet.detail.checked === true) {
      this.vcovidno = false;
      this.vcovidsi = true;
    } else {
      this.vcovidno = true;
      this.vcovidsi = false;
    }
  }
 
  vcovidnoe(evnet: any) {
    if (evnet.detail.checked === true) {
      this.vcovidno = true;
      this.vcovidsi = false;
    } else {
      this.vcovidno = false;
      this.vcovidsi = true;
    }
  }

}
