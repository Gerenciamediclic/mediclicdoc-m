import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-firmas',
  templateUrl: './firmas.page.html',
  styleUrls: ['./firmas.page.scss'],
})
export class FirmasPage implements OnInit {

  @ViewChild('singature' ,  {static : true}) signaturePad: SignaturePad;
  @Input() nombre;
 
  imagen: any;

  // tslint:disable-next-line: ban-types
  signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    minWidth: 2,
    canvasWidth: 500,
    canvasHeight: 300
  };
 

  constructor(
    private modalcontrol: ModalController
  ) {}



  ngOnInit() {
  }

  ngAfterViewInit() {
    // this.signaturePad is now available
     this.signaturePad.set('minWidth', 2); // set szimek/signature_pad options at runtime
     this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
  }
 
  drawComplete() {
    // will be notified of szimek/signature_pad's onEnd event
    console.log(this.signaturePad.toDataURL());
    this.imagen = this.signaturePad.toDataURL();
  }
 
  drawStart() {
    // will be notified of szimek/signature_pad's onBegin event
    console.log('begin drawing');
  }

 
  borrar() {
    this.signaturePad.clear();
  }

  guardar() {
    this.modalcontrol.dismiss(
      this.signaturePad.toDataURL()
    );

  }

 
}

