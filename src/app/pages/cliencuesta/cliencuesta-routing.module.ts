import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CliencuestaPage } from './cliencuesta.page';

const routes: Routes = [
  {
    path: '',
    component: CliencuestaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CliencuestaPageRoutingModule {}
