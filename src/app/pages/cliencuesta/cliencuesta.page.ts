import { Component, OnInit } from '@angular/core';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { Usuario } from '../../../model/usuario.model';
import { Encuestabio } from '../../../model/encuestabio.model.';
import { UiserviceService } from '../../services/Uiservice/uiservice.service';
import { EncuestabioService } from '../../services/encuestabio/encuestabio.service';
import { NavController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-cliencuesta',
  templateUrl: './cliencuesta.page.html',
  styleUrls: ['./cliencuesta.page.scss'],
})
export class CliencuestaPage implements OnInit {

scannedData: any;
encodedData: '';
encodeData: any;
datajson: any;
textdate: any;
cedula: string;
apellido1: string;
apellido2: string;
nombre1: string;
nombre2: string;
fechnac: string;
sangre: string;
sexo: string;


todo: any;

  salirsi: boolean;
  salirno: boolean;
  vcovidsi: boolean;
  vcovidno: boolean;
  usuario: Usuario;

  encuestabio = new Encuestabio(
    null
  );

  constructor(
    public barcodeCtrl: BarcodeScanner,
    private uiservice: UiserviceService,
    private encuservice: EncuestabioService,
    private navcontr: NavController,
    
    ) { }

  ngOnInit() {
    this.salirsi = false;
    this.salirno = false;
    this.vcovidsi = false;
    this.vcovidno = false;
    this.usuario = JSON.parse( localStorage.getItem('usuariomovil') );
    console.log('usuario', this.usuario);
   
  }

  encuestac(f: NgForm) {
    console.log(f);
    
    if ( this.encuestabio.ninguno === undefined ) {
        if ( this.encuestabio.tos === undefined ) {
          if ( this.encuestabio.drespirar === undefined ) {
            if ( this.encuestabio.dolorcabeza === undefined ) {
              if ( this.encuestabio.dolorgarganta === undefined ) {
                if ( this.encuestabio.faltagusto === undefined ) {
                  this.uiservice.alertainformativa('¡Debe seleccionar un Campo!');
                } else {
                  this.encuestabio.alerta = true;
                  this.validar0();
                }
              } else {
                this.encuestabio.alerta = true;
                this.validar0();
              }
            } else {
              this.encuestabio.alerta = true;
              this.validar0();
            }
          } else {
            this.encuestabio.alerta = true;
            this.validar0();
          }
        } else {
          this.encuestabio.alerta = true;
          this.validar0();
        }

    } else {
      this.encuestabio.alerta = false;
      this.validar0();
    }
  }


  validar0() {
    if ( this.salirsi === true) {
      this.encuestabio.spais14 = true;
      this.encuestabio.alerta = true;
      this.validar1();
} else {
    if ( this.salirno === true) {
      this.encuestabio.spais14 = false;
      this.validar1();
    } else {
      this.uiservice.alertainformativa('¡Debe seleccionar un Campo Viaje Al Exterior!');
    }
    }
  }

  validar1() {
    if ( this.vcovidsi === true) {
      this.encuestabio.famcovid = true;
      this.encuestabio.alerta = true;
      this.validartem();
    } else {
   if ( this.vcovidno === true) {
    this.encuestabio.famcovid = false;
    this.validartem();
   } else {
     this.uiservice.alertainformativa('¡Debe seleccionar un Campo COVID!');
   }
  }
  }

  validartem() {
    if ( this.encuestabio.temperatura <= 32  || this.encuestabio.temperatura >= 45) {
      this.uiservice.alertainformativa('¡Rango De temperatura errado!');
    } else {
      if ( this.encuestabio.temperatura >= 38 ) {
        this.uiservice.alertainformativa('!Se Solicita Aislamiento Preventivo!');
        this.encuestabio.alerta = true;
        this.guardarform();
      } else {
        this.guardarform();
      }
  }
}

  guardarform() {
    this.encuestabio.estado = true;
    //this.encuestabio.name = this.usuario.name;
    //this.encuestabio.identificacion = this.usuario.identificacion;
    this.encuestabio.email = this.usuario.email;
    this.encuestabio.cliente = this.usuario.cliente;
    this.encuestabio.idclient = this.usuario.idclient;
    this.encuestabio.date = new Date().getTime();
    //this.encuestabio.phone = this.usuario.phone;
    this.encuestabio.idgrupo  = this.usuario.idgrupo;
    this.encuestabio.grupo  = this.usuario.grupo;
    this.encuestabio.tipoenc = 'Visitante';
    const dataa = JSON.parse (JSON.stringify (this.encuestabio));
    this.encuservice.crearEncuestabio(dataa).then(resp => {
      this.uiservice.alertainformativa('¡Se Ha Guardado Con Exito');
      this.navcontr.navigateRoot('/tabs/tabs/visitantes');
    });
  }

  


  salirsie(evnet: any) {
    console.log(evnet);
    if (evnet.detail.checked === true) {
      this.salirno = false;
      this.salirsi = true;
    } else {
      this.salirno = true;
      this.salirsi = false;
    }
    
  }
 
  salirnoe(evnet: any) {
    if (evnet.detail.checked === true) {
      this.salirno = true;
      this.salirsi = false;
    } else {
      this.salirno = false;
      this.salirsi = true;
    }
  }

  vcovidsie(evnet: any) {
    console.log(evnet);
    if (evnet.detail.checked === true) {
      this.vcovidno = false;
      this.vcovidsi = true;
    } else {
      this.vcovidno = true;
      this.vcovidsi = false;
    }
  }
 
  vcovidnoe(evnet: any) {
    if (evnet.detail.checked === true) {
      this.vcovidno = true;
      this.vcovidsi = false;
    } else {
      this.vcovidno = false;
      this.vcovidsi = true;
    }
  }

  goToBarcodeScan() {
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false,
      showFlipCameraButton: true,
      showTorchButton: true,
      torchOn: false,
      prompt: 'Coloque la Cedula dentro Del Recuadro',
      resultDisplayDuration: 500,
      formats: 'QR_CODE,PDF_417 ',
      orientation: 'landscape',
      disableAnimations : false, // iOS
      disableSuccessBeep: false // iOS and Android
    };

    this.barcodeCtrl.scan(options).then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.scannedData = barcodeData;
      this.datajson =  JSON.parse (JSON.stringify (barcodeData));
      this.textdate = barcodeData.text;

      this.cedula = barcodeData.text.substr(48, 10);
      this.apellido1 = this.escapeRegExp(barcodeData.text.substr(58, 23));
      this.apellido2 = this.escapeRegExp(barcodeData.text.substr(81, 23));
      this.nombre1 = this.escapeRegExp(barcodeData.text.substr(104, 23));
      this.nombre2 = this.escapeRegExp(barcodeData.text.substr(127, 23));
      this.sexo = this.escapeRegExp(barcodeData.text.substr(150, 2));
      this.fechnac = barcodeData.text.substr(152, 8);
      this.sangre = barcodeData.text.substr(166, 2);

      this.encuestabio.identificacion = parseInt(this.cedula, 10);
      const nombre = this.nombre1 +  ' ' + this.nombre2 + ' ' + this.apellido1 + ' ' + this.apellido2;
      this.encuestabio.name = nombre;


    }).catch(err => {
      console.log('Error', err);
    });
  }

 escapeRegExp(fb: string) {
    return fb.replace(/ /g, '');
  }

}
