import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CliencuestaPageRoutingModule } from './cliencuesta-routing.module';

import { CliencuestaPage } from './cliencuesta.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CliencuestaPageRoutingModule
  ],
  declarations: [CliencuestaPage]
})
export class CliencuestaPageModule {}
