import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CliencuestaPage } from './cliencuesta.page';

describe('CliencuestaPage', () => {
  let component: CliencuestaPage;
  let fixture: ComponentFixture<CliencuestaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CliencuestaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CliencuestaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
