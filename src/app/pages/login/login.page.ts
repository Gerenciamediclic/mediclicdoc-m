import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IonSlides, NavController } from '@ionic/angular';
import { UsuariosService } from '../../services/usuarios/usuarios.service';
import { Usuario } from 'src/model/usuario.model';
import { UiserviceService } from '../../services/Uiservice/uiservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  @ViewChild('slidePrincipal', {static: false}) slides: IonSlides;
  usuario = new Usuario (
    ''
  );

  avatars = [
    {
      img: 'av-1.png',
      seleccionado: true
    },
    {
      img: 'av-2.png',
      seleccionado: false
    },
    {
      img: 'av-3.png',
      seleccionado: false
    },
    {
      img: 'av-4.png',
      seleccionado: false
    },
    {
      img: 'av-5.png',
      seleccionado: false
    },
    {
      img: 'av-6.png',
      seleccionado: false
    },
    {
      img: 'av-7.png',
      seleccionado: false
    },
    {
      img: 'av-8.png',
      seleccionado: false
    },
];

avatarSlide = {
  slidesPerView: 3.5
};

  constructor(
    private loginservice: UsuariosService,
    private uiservice: UiserviceService,
    private navcontr: NavController
  ) { }

  ngOnInit() {
    // this.slides.lockSwipes(true);
  }

  login( fLogin: NgForm ) {
    this.loginservice.login(this.usuario).then(resp  => {
          console.log('logiado', resp);
          this.loginservice.buscarUsuarios(this.usuario.email).then(respus => {
              console.log(respus);
              if (respus.length === 0 ) {
                this.uiservice.alertainformativa('¡Usuario No Existe!');
              } else {
                this.loginservice.guardarStorage(respus[0]);
                this.navcontr.navigateRoot('/tabs/tabs/encuesta');
              }
          });



    }).catch( respc => {
          console.log('Error', respc);
          this.uiservice.alertainformativa('¡Usuario o Contraseña Invalida!');
    });

  }

  registro( fregistro: NgForm ) {
    console.log( fregistro.valid );
  }

  seleccionarAvatar( avatar ) {

    this.avatars.forEach( av => av.seleccionado = false );
    avatar.seleccionado = true;

  }

  mostrarRegistro() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(0);
    this.slides.lockSwipes(true);
  }

  mostrarLogin()  {
    this.slides.lockSwipes(false);
    this.slides.slideTo(1);
    this.slides.lockSwipes(true);
  }

}
